import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/timer',
    name: 'timer',
    component: () => import('../views/timer/TimerView.vue')
  },
  {
    path: '/projects',
    name: 'projects',
    component: () => import('../views/projects/ProjectsListView.vue')
  },
  {
    path: '/project/create',
    name: 'project-create',
    component: () => import('../views/projects/ProjectFormView.vue')
  },
  {
    path: '/clients',
    name: 'clients',
    component: () => import('../views/clients/ClientsListView.vue')
  },
  {
    path: '/client/create',
    name: 'client-create',
    component: () => import('../views/clients/ClientFormView.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/auth/LoginView.vue')
  },
  {
    path: '/signup',
    name: 'signup',
    component: () => import('../views/auth/SignupView.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.name === 'login' || to.name === 'signup'){
    next()
  } else {
    const userId = localStorage.getItem('userId')
    if (userId == null || userId == undefined) {
      next({name: 'login'})
    } 
    next()
  }
})

export default router

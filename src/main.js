import Vue from 'vue'
import App from './App.vue'
import router from './router'

import axios from 'axios'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

import VSwatches from 'vue-swatches'
import 'vue-swatches/dist/vue-swatches.css'

Vue.use(VSwatches)
Vue.use(Buefy)

axios.defaults.baseURL = 'http://localhost:5000/v1/'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

Vue.config.productionTip = false

new Vue({
  router,
  render: function (h) { return h(App) }
}).$mount('#app')
